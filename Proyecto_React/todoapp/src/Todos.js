const Todos = ({ todos, deleteTodos }) => {
    const todoList = todos.length ? (
        todos.map(x => {
            return (
                <div className="collection-item" key={x.id}>
                    <span onClick={() => {deleteTodos(x.id)}}>{x.content}</span>
                </div>
            )
        })
    ) : (
        <p className="center"> U have Completed it all! &#127882; </p>
    )
    return (
        <div className="todos collection">
            { todoList }
        </div>
    )
}
export default Todos;