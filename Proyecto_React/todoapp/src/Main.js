import React, { Component } from 'react';
import Todos from './Todos';
import AddTodo from './AddTodo';

class Main extends Component{
    state = {
        todos:[
            {id: 1, content: 'have a breakfast'},
            {id:2 , content: 'Take the metro'}
        ]
    }
    addTodo = (todo) => {
        todo.id = Math.max(...this.state.todos.map(x => x.id)) + 1;
        let allTodos =[...this.state.todos, todo];
        this.setState({
            todos: allTodos
        })
    }
    deleteTodo = (id) =>{
        let allTodos = this.state.todos.filter( x => x.id !== id);
        this.setState({
            todos: allTodos
        })
    }
    render(){
        return(
            <div className="todo-app container">
                <h1 className = "center blue-text">Todo's</h1>
                <Todos deleteTodos={this.deleteTodo} todos={this.state.todos}/>
                <AddTodo addTodo={this.addTodo}/>
            </div>
        )
    }

}
export default Main;