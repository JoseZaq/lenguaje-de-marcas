import React, {Component} from 'react';

class Food extends Component{
    render(){
        const{name, type}= this.props;
        return(
            <div>
                <div>Name: {name}</div>
                <div>Type: {type}</div>
            </div>
        )
    }
}
export default Food;