import React from 'react';
import './Person.css';

const Person = ({ persons, deletePerson }) => {
    // esto es un ui component, no tiene state, sirve mas para estetica que practica
    const personsList = persons.map(x => {
        return x.age >20 ? (
            <div className="Person" key={x.key}>
                <div>Name: {x.name}</div>
                <div>Age: {x.age}</div>
                <button onClick={() => {deletePerson(x.key)}} > Delete Person</button>
            </div>
        ) : null;
    })
    return (
        <div className="person-list">
            {personsList}
            
        </div>
    )
}
export default Person;