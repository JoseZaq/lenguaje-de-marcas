import React, { Component } from 'react';
import Person from './Person';
import Food from './Food';
import AddPerson from './AddPerson';
class Principal extends Component {
    state = {
        persons: [
            { name: "El Gato", age: "25", key: 1 },
            { name: "El Machu", age: "15", key: 2 },
            { name: "El Jose", age: "23", key: 3 }
        ]
    }
    addPerson = (person) => {
        let maxKey = Math.max(...this.state.persons.map(x => x.key));
        person.key = maxKey + 1;
        let allPersons = [...this.state.persons, person];
        this.setState({
            persons: allPersons
        })
        console.log(this.state);
    }
    deletePerson = (key) => {
        let allPersons = this.state.persons.filter(x => x.key !== key);
        this.setState({
            persons: allPersons
        });
    }
    render() {
        return (
            <div className="App">
                <h1>The Best exercice to learn React </h1>
                <h2>Persons</h2>
                <Person deletePerson={this.deletePerson} persons={this.state.persons} />
                <h2>Foods</h2>
                <Food name="Naranja" type="Citrico" />
                <Food name="Almendras" type="Verduras" />
                <h2>Add Person</h2>
                < AddPerson addPerson={this.addPerson} />
            </div>
        );
    }
}
export default Principal;