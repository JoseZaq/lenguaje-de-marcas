import React, { Component } from 'react';
import NavBar from './components/Navbar';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './components/home';
import Contact from './components/Contact';
import About from './components/About';
import Post from './components/Post';
class Main extends Component {
    render() {
        //<!--Para que funcione el router de paginas se debe instalar npm install react-router-dom --!>
        // Switch obliga a solo usar uno de los tags que tiene dentro, impide que se observe un post y otra pagina a la vez
        return (
            <BrowserRouter>
                <div className="the-Main">
                    <NavBar />
                    <Switch>
                        <Route exact path="/" component={Home} />
                        <Route path="/about" component={About} />
                        <Route path="/contact" component={Contact} />
                        <Route path="/:post_id" component={Post} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}
export default Main;