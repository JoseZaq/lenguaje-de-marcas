import React, { Component } from 'react';
import axios from 'axios';

class Post extends Component {
    state = {
        post: null
    }
    componentDidMount() {
        //overide
        let id = this.props.match.params.post_id;
        axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
            .then(x => {
                this.setState({
                    post: x.data
                })
            })
    }
    render() {
        const post = this.state.post ? (
            <div className="post">
                <h2 className="center">{this.state.post.title}</h2>
                <p>{this.state.post.body}</p>
            </div>
        ) : (
            <div className="center">
                Loading Post...
            </div>
        )
        return (
            <div className="container">
                {post}
            </div>
        )
    }
}
export default Post;