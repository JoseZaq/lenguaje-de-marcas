const Contact = (props) => {
    setTimeout(() => {
        props.history.push('/about')
    }, 2000) // redirige despues de 2 segundos a la pagina de about :)
    return (
        <div className="Contact">
            <h2>Contact</h2>
            <p>orem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper quam nec orci gravida, ut posuere velit sagittis. Suspendisse potenti. Vestibulum vitae pharetra justo, ut vulputate libero. Duis ante ex, aliquam et tempus sed, mollis non velit. Cras aliquam nulla ut quam scelerisque consequat. Duis et leo in sem posuere semper. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam in massa rhoncus, malesuada enim eu, blandit nibh. Sed neque leo, suscipit quis arcu id, vulputate tincidunt urna. Sed mattis imperdiet mauris ut blandit. Suspendisse potenti. In fermentum massa ac sodales fringilla. Sed tristique tortor quis tempor vulputate.

                Integer sodales augue porta, ultrices nibh id, tincidunt nisi. Nullam ullamcorper aliquam ultricies. Suspendisse malesuada scelerisque turpis a vehicula. Praesent consequat, velit ac mattis varius, velit est sodales lectus, non fringilla felis lorem et augue. Nam eu ligula ante. Sed auctor ac ligula sit amet volutpat. Sed consequat bibendum massa quis ultricies. Nunc sagittis tortor non magna pulvinar accumsan.

                Ut id condimentum metus. Fusce bibendum tristique ante, vitae suscipit quam bibendum vitae. Ut facilisis erat eu odio blandit congue. Etiam at fringilla nisl, ac mollis neque. Donec tincidunt lacus quis orci hendrerit, in malesuada felis luctus. Nullam fringilla a leo nec ornare. Suspendisse arcu sapien, varius sed lacinia ac, imperdiet non neque. Phasellus tortor lectus, semper iaculis tortor a, tincidunt euismod erat. Sed mollis lacus orci, sed ultricies odio mollis maximus. Phasellus ultricies, leo at cursus dignissim, lectus dui sollicitudin diam, quis suscipit est enim vel orci. Integer mollis eget nisl vitae viverra.

                Sed gravida vestibulum nunc quis sollicitudin. Duis tincidunt nulla vel metus mollis, non posuere neque blandit. Aliquam erat volutpat. Proin ac gravida lorem, eu condimentum lacus. Nunc interdum neque sit amet ex eleifend, in rhoncus purus vestibulum. Morbi vitae ex ut risus consectetur tempus. Aliquam a semper ex. Pellentesque rhoncus ex non lacus pretium lobortis. Fusce venenatis, erat sit amet venenatis molestie, massa nunc sagittis arcu, ut interdum neque lorem eget tellus. Ut volutpat tellus et sagittis rhoncus. Ut id posuere enim. Sed eu urna eget eros pellentesque sollicitudin.

                Mauris blandit dictum rutrum. Etiam et est in ligula laoreet laoreet. Ut ullamcorper leo ac felis tincidunt, eu semper velit mattis. Sed et viverra nisi. Donec posuere lectus et massa hendrerit malesuada ac a magna. Fusce laoreet arcu leo, id sagittis velit venenatis in. Etiam eu tristique erat. Quisque nec pretium dui. Vestibulum elementum in quam ut congue. Fusce purus ante, vestibulum ultricies mollis nec, lacinia eu arcu. Praesent sed nunc tortor. Quisque finibus nisl ac viverra congue. Nam elementum ante sit amet odio viverra, in vehicula lacus tincidunt. Aliquam erat volutpat.
            </p>
        </div>
    )
}
export default Contact;