import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Image from '../image.png';

class Home extends Component {
    state = {
        posts: []
    }
    // Para usar datos de fuera de la app, se instalo axios -> npm install axios
    // data extract from the page -> https://jsonplaceholder.typicode.com
    componentDidMount() {
        // @overwrite
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(x => {
                this.setState({
                    posts: x.data.slice(0, 10) // slice -> sirve para coger los primeros 10 elementos unicamente
                })
            })
    }
    render() {
        const { posts } = this.state; // porque no cojo directo del state? 
        const postList = posts.length ? (
            posts.map(x => {
                return (
                    <div className="post card" key={x.id}>
                        <img src={Image} width="200px"/>
                        <div className="card-content">
                            <Link to={"/" + x.id}>
                                <span className="card-title blue-text">{x.title}</span>
                            </Link>
                            <p>{x.body}</p>
                        </div>
                    </div>)
            })
        ) : (
            <div className="center"> No post yet</div>
        )
        return (
            <div className="Home">
                <h2>Home</h2>
                {postList}
            </div>
        )
    }
}
export default Home;