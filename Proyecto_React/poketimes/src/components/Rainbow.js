const Rainbow = (WrappedComponent) => {
    const colors = ['orange', 'blue', 'red', 'pink', 'purple'];
    const randomColors = colors[Math.floor(Math.random() *  5)];
    const className = randomColors + '-text';
    return(props) =>{
        return(
            <div className={className}>
                <WrappedComponent {...props}/>
            </div>
        )
    }
}
export default Rainbow;