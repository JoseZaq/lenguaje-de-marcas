# Guia para Plantillas de Email
## Base html
### Porque usar esta estructura ?
El gran problema con las plantillas de correos es su compatibilidad con los diferentes motores de correo
debido a que estas no funcionan con las ultimas versiones de html y css.

Debido a esto, se deben usar algunas lineas de codigo para que no haya errores de visualizacion
del correo en los diferentes motores.

Para mas detalles de cada linea de codigo pincha [aqui](https://webdesign.tutsplus.com/tutorials/creating-a-future-proof-responsive-email-without-media-queries--cms-23919)

### Base
Simplificando un poco, se tienen preestablecidos los tags header y body 

El contenido de la plantilla debe reemplazar a
```
[content goes here]
```

```
<!DOCTYPE html>
<html lang="en" xmlns="https://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <title></title>
    <!--[if mso]>
    <style type="text/css">
        table {border-collapse:collapse;border:0;border-spacing:0;margin:0;}
        div, td {padding:0;}
        div {margin:0 !important;}
    </style>
    <noscript>
        <xml>
            <o:OfficeDocumentSettings>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
    </noscript>
    <![endif]-->
</head>
<body style="margin:0;padding:0;word-spacing:normal;background-color:#ffffff;">
<div role="article" aria-roledescription="email" lang="en" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#ffffff;">
    <table role="presentation" style="width:100%;border:0;border-spacing:0;">
        <tr>
            <td align="center">
                <!--[if mso]>
                <table role="presentation" align="center" style="width:660px;">
                    <tr>
                        <td style="padding:20px 0;">
                <![endif]-->
                <div class="outer" style="width:96%;max-width:660px;margin:20px auto;">
                    [content goes here]
                </div>
                <!--[if mso]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </td>
        </tr>
    </table>
</div>
</body>
</html>
```
## Maquetizacion
Para ayudarte en el diseño de la plantilla es conveniente visualizar los bordes de las tablas, filas y columnas
```css
/*TEMP*/
table, tr, td {
 border: 1px solid black;
}
```
Una vez terminado borrarla
## Compatibilidad
los motores de correos mas problematicos son Outlook y Gmail.

Para evitar cualquier error se debe trabajar con versiones antiguas de CSS y HTML
( trabajar con tablas y estilos basicos)

Para conocer que atributos o estilos son compatibles en emails
existen varias herramientas en internet, una muy completa es [https://www.caniemail.com](https://www.caniemail.com/features/css-unit-vw/)

## Estilos
La mayoria de propiedades comunes de css que se usan en una pagina web no
son compatibles con una plantilla de correos, por lo que hay que estar atentos que se puede o no usar

Para conocer que atributos o estilos son compatibles en emails
existen varias herramientas en internet, una muy completa es [https://www.caniemail.com](https://www.caniemail.com/features/css-unit-vw/)

Evitar sobre todo lo siguiente:

- shorhands
    - MAL:
  ```html
  padding: 10px 0px
  ```
    - BIEN:
```html
  padding-top: 10px, padding-bottom:10px, padding-left: 0px, padding-rihgt: 0px
```

## Tags Pre-construidos
Muchos de los tags construidos tienen un mismo estilo, por lo que para
facilitar la construccion de la plantilla he construido tags con estilos comunes:
```html

# P STYLE
<p style="font-family:proxima-nova,sans-serif;font-size: 14px;color:black;padding:0;margin:0 "> </p>

# SPACER
<div class="spacer" style="line-height:26px;height:26px;mso-line-height-rule:exactly;">&nbsp;</div>

# table

<table role="presentation" style="width:100%;border:0;border-spacing:0;">
    <tr>
        <td>
        </td>
    </tr>
</table>

# img
<img src=" " width="600" style="width: 100%;max-width:600px;height: auto; display: block;"/>
# div-img
<div class="" style="background-image: url('some url');background-size: 36px;background-repeat:no-repeat;width:36px;height: 40px; display: inline-block;"> </div>
```

## Responsive
El mejor metodo para crear una plantilla responsive es la llamada fluid-hybrid

Consiste en establecer tamaños a los contenedores con porcentajes y limitarlos con un max-width/height

Para mas informacion -> [aqui](https://webdesign.tutsplus.com/tutorials/creating-a-future-proof-responsive-email-without-media-queries--cms-23919)

## Problemas comunes
### centrar contenidos
En algunos casos margin: auto no es compatible por lo que se debe usar lo siguiente:

#### centrar horizontalmente

se aplica centrado de texto en la contendor padre, y un display: inline-block
en el contenedor hijo para que reaccione al centrado de texto
```html
# set horizontal center
<td style="text-align: center">
    <some style="display:inline-block"/>
<td/>
```

#### vertical align
valign es un attr de tablas para el alineamiento vertical del contenido.

Adicionalmente, existe la prop vetical-align para objetos inline-block o table-cell
```html
# set vertical align
<td style="vertical-align: middle" valign:"middle">
<some />
<td/>
```

### bucles
En el caso de tener una seccion donde varien sus items segun las variables entregadas por Django,
por ejemplo los tipos de comida descartadas por el cliente, se debe establecer un diseño semenjante a:
```css
display: flex;
flex-wrap: wrap;
```
Para eso se puede usar una tabla con una unica fila con los items configurados para que se pongan alado de la otra y no abajo
con display: inline-block:
```html
<table align="center" role="presentation" style="width:100%;border:0;border-spacing:0;text-align: center">
    <tr>
        <td style="padding-top: 15px;padding-bottom: 15px;" valign="middle">
            <!--here goes a loop -->
            <div class="foodItem" style="display: inline-block;padding-left: 2px;padding-right: 2px;padding-top: 2px;padding-bottom: 2px;"
                 [some content]
            </div>
            <!--end-->
        </td>
    </tr>
</table>
```
## Testeo
Mailgun es una pagina web que permite, entre otras cosas, testear plantillas de email.

Para eso hay que subir la plantilla en la pestaña Sending->Templates
![img.png](img.png)

Una vez ahi seleccionar "create message template" para crear una nueva plantilla.
Ahi pegas el contenido html y guardas

Para poder enviar un correo de la plantilla a una cuenta de email
se tiene que tener registrada esta cuenta en el dominio de
sandbox416269310a314c58bf59be1b1d30cdec.mailgun.org. Esto se puede verificar llendo a 
Sending->Domains->(nombre del dominio)

Si no se encuentra registrada hay que pedir al administrador de mailgun que lo añada

Para enviar la plantilla se tiene varias opciones, la mas sencilla es por medio
de la terminal del ordenador.
Se tiene un ejemplo del codigo en la plantilla guardada en mailgun que es como la siguiente:
```
curl -s --user 'api:ENTER_API_KEY_HERE' \
	 https://api.mailgun.net/v3/sandbox416269310a314c58bf59be1b1d30cdec.mailgun.org/messages \
	 -F from='Mailgun Sandbox <postmaster@sandbox416269310a314c58bf59be1b1d30cdec.mailgun.org>' \
	 -F to='NAME OF RECEIVER  <THE EMAIL TO SEND>' \
	 -F subject='Hello YOUR NAME' \
	 -F template='TEMPLATE NAME' \
	 -F h:X-Mailgun-Variables='{"test": "test"}'
# Send an email using your active template with the above snippet
# You can see a record of this email in your logs: https://app.mailgun.com/app/logs.
```

La api se puede obtener en Sending->Domains->(nombre del dominio)->API.

En nuestro caso será siempre
```
API KEY -> bc1b9971f08512487e433fcc3be700bd-4de08e90-5d04785b
```


## Anexos
- guia completa de construccion de plantillas de email 
[https://webdesign.tutsplus.com/tutorials/creating-a-future-proof-responsive-email-without-media-queries--cms-23919](https://webdesign.tutsplus.com/tutorials/creating-a-future-proof-responsive-email-without-media-queries--cms-23919)
- buenas practicas en html emails
[https://www.imaginanet.com/blog/buenas-practicas-para-la-creacion-de-html-para-emails.html](https://www.imaginanet.com/blog/buenas-practicas-para-la-creacion-de-html-para-emails.html)
